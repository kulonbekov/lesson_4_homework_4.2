package com.company;

import java.util.Random;

public class Main {

    public static int[] health = {700, 250, 250, 250, 250};
    public static int[] hits = {50, 20, 20, 20, 20};
    public static String[] hitTypes = {"Physical", "Physical", "Magical", "Mental", "Tor"};

    public static void main(String[] args) {
        while (!isFinish()) {
            changeBossDefence();
            round();
            printStatistics();
        }
    }

    public static void changeBossDefence() {
        Random r = new Random();
        int randomNum = r.nextInt(3) + 1;
        hitTypes[0] = hitTypes[randomNum];
    }

    public static void printStatistics() {

        System.out.println("___________________________");
        System.out.println("Boss health: " + health[0]);
        System.out.println("Warrior health: " + health[1]);
        System.out.println("Magic health: " + health[2]);
        System.out.println("KInetic health: " + health[3]);
        System.out.println("Tor health: " + health[4]);
        System.out.println("___________________________");
    }
    public static void round() {
        for (int i = 1; i <= 4; i++) {
            if (health[0] > 0) {
                int damagedHealth = playerHit(i);
                if (damagedHealth < 0) {
                    health[0] = 0;
                } else {
                    health[0] = damagedHealth;
                }
            }
        }
        if (health[0] > 0) {
            int udarTora = torhit(4); //Если удар Tora равна 40, то Босс пропускает один раунд
            for (int i = 1; i <= 4; i++) {

                if (health[i] > 0) {
                    if (udarTora == 40) { //Здесь условия IF проверяет удар тора
                        continue;
                    } else {
                        int damagedHealthHerous = bossHit(i);

                        if (damagedHealthHerous < 0) {
                            health[i] = 0;
                        } else {
                            health[i] = damagedHealthHerous;
                        }
                    }
                }
            }
        }
    }

    public static boolean isFinish() {
        if (health[0] <= 0) {

            System.out.println("Herous won!!!");
            return true;
        }
        if (health[1] <= 0 && health[2] <= 0 && health[3] <= 0 && health[4] <= 0) {
            System.out.println("Boss won!!!");
            return true;
        }
        return false;
    }

    public static int playerHit(int playerIndex) {
        Random r = new Random();
        int randomNumber = r.nextInt(5) + 1;
        if (hitTypes[0].equals(hitTypes[playerIndex])) {
            System.out.println(hitTypes[playerIndex] + " hits:" + hits[playerIndex] * randomNumber);
            return health[0] - hits[playerIndex] * randomNumber;
        } else {
            return health[0] - hits[playerIndex];
        }
    }

    public static int bossHit(int playerIndex) {

        /*Random p = new Random();
        int randomNumberBoss = p.nextInt(5) + 1;
        if (hitTypes[0].equals(hitTypes[playerIndex])) {
            System.out.println("Boss hits:" + hits[playerIndex] * randomNumberBoss);
            return health[playerIndex] - hits[playerIndex];
        } else {*/

        return health[playerIndex] - hits[0];
    }

    public static int torhit(int playerndex) {  //Этот метод формирует удар Тора через рандом
        Random t = new Random();
        int torhitrandom = t.nextInt(2) + 1;

        int torhit1 = hits[4] * torhitrandom;
        System.out.println(hitTypes[4] + " hits:" + torhit1);
        return torhit1;


    }
}

